#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "rclcpp/rclcpp.hpp"
#include "trajectory_generation_lib.h"
#include "ros2_planar_robot/msg/example1.hpp"
#include "ros2_planar_robot/msg/example2.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Trajectory generator node 
class TrajGen : public rclcpp::Node
{
  public:
    TrajGen():
      Node("traj_gen")
      {      
        // Publisher on current desired state (to be used for each milisecond)
        pub_desired_pose_ = this->create_publisher    <ros2_planar_robot::msg::Example2>(
            "desired_state",10
        );
        // Subscribe on nxt_pose topic, managed by high_lev_mng node
        subs_next_pose_   = this->create_subscription <ros2_planar_robot::msg::Example1>   (
            "nxt_pose", 10, std::bind(&TrajGen::topic_callback, this, _1)
        );
      }
 
  private:
    // Publishes current desired pose depending on the states awayting_
    void timer_callback(){
      auto message = ros2_planar_robot::msg::Example2();
      message.ex2.x = 1;
      message.ex2.y = 2;
      message.ex3   = {3, 4, 5, 6};
      pub_desired_pose_->publish(message);
    }

    // Update polynomials and states based on next pose
    void topic_callback(const ros2_planar_robot::msg::Example1::SharedPtr ref_pose)    {
      double d1 = ref_pose->x;  
      double d2 = ref_pose->y;  
      RCLCPP_INFO(this->get_logger(), "Received: x = '%f', y = '%f'", d1, d2);    
    }

    Point2Point p2p{Eigen::Vector2d::Zero(),Eigen::Vector2d(1,1),3};
    bool          running_            { false                   } ;
    bool          awaiting_           { true                    } ;
    double        Dt                  { 0                       } ;
    const double  dt                  { 1e-3                    } ;
    double        t                   { 0                       } ;
    Eigen::Vector2d x_next_     { Eigen::Vector2d::Zero() } ;
    Eigen::Vector2d x_previous_ { Eigen::Vector2d::Zero() } ;
    rclcpp::TimerBase::SharedPtr                                      timer_                                          ;
    rclcpp::Publisher<ros2_planar_robot::msg::Example2>::SharedPtr    pub_desired_pose_                               ;   
    rclcpp::Subscription<ros2_planar_robot::msg::Example1>::SharedPtr subs_next_pose_                                 ;        
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<TrajGen>());  
  rclcpp::shutdown();
  return 0;
}